'use strict';

module.exports = {
  env: {
    'browser' : true,
    'es6'     : true,
    'node'    : true,
  },
  parserOptions: {
    ecmaFeatures : { experimentalObjectRestSpread: true },
    ecmaVersion  : 2017,
    sourceType   : 'module',
  },
  rules: {
    'array-bracket-newline' : [2, { 'multiline': true }],
    'array-bracket-spacing' : 2,
    'arrow-body-style'      : 2,
    'arrow-parens'          : 2,
    'arrow-spacing'         : 2,
    'block-spacing'         : [2, 'never'],
    'brace-style'           : [2, 'stroustrup'],
    'camelcase'             : 0,
    'comma-dangle'          : [2, 'always-multiline'],
    'consistent-return'     : 2,
    'curly'                 : [2, 'all'],
    'dot-location'          : [2, 'property'],
    'dot-notation'          : 2,
    'eol-last'              : 2,
    'eqeqeq'                : [2, 'always'],
    'indent'                : [
      2, 2, {
        'ArrayExpression'    : 1,
        'ObjectExpression'   : 1,
        'SwitchCase'         : 1,
        'VariableDeclarator' : 2,
      },
    ],
    'jsx-quotes'  : [2, 'prefer-single'],
    'key-spacing' : [
      2, {
        'align': {
          'afterColon'  : true,
          'beforeColon' : true,
          'on'          : 'colon',
        },
        'singleLine': {
          'afterColon'  : true,
          'beforeColon' : false,
        },
      },
    ],
    'keyword-spacing'       : 2,
    'new-cap'               : 0,
    'no-alert'              : 1,
    'no-console'            : [1, { 'allow': ['warn', 'error'] }],
    'no-delete-var'         : 2,
    'no-duplicate-imports'  : 2,
    'no-extra-bind'         : 2,
    'no-extra-boolean-cast' : 2,
    'no-extra-parens'       : [
      2, 'all', {
        'ignoreJSX'               : 'multi-line',
        'nestedBinaryExpressions' : false,
      },
    ],
    'no-extra-semi'            : 2,
    'no-global-assign'         : 2,
    'no-implicit-coercion'     : 2,
    'no-invalid-this'          : 2,
    'no-mixed-requires'        : 2,
    'no-mixed-spaces-and-tabs' : 2,
    'no-multi-spaces'          : [
      2, {
        'exceptions': {
          'ImportDeclaration'  : true,
          'VariableDeclarator' : true,
        },
      },
    ],
    'no-multiple-empty-lines'       : [2, { 'max': 1 }],
    'no-redeclare'                  : 2,
    'no-regex-spaces'               : 2,
    'no-self-assign'                : 2,
    'no-self-compare'               : 2,
    'no-tabs'                       : 2,
    'no-trailing-spaces'            : 2,
    'no-undef'                      : 2,
    'no-undef-init'                 : 2,
    'no-undefined'                  : 1,
    'no-underscore-dangle'          : 0,
    'no-unneeded-ternary'           : 2,
    'no-unsafe-negation'            : 2,
    'no-unused-labels'              : 2,
    'no-unused-vars'                : 2,
    'no-use-before-define'          : 2,
    'no-var'                        : 2,
    'no-whitespace-before-property' : 2,
    'object-curly-newline'          : 2,
    'object-curly-spacing'          : [2, 'always'],
    'object-property-newline'       : 2,
    'one-var-declaration-per-line'  : 2,
    'operator-assignment'           : 2,
    'padded-blocks'                 : [
      2, {
        'blocks'   : 'never',
        'classes'  : 'always',
        'switches' : 'always',
      },
    ],
    'prefer-const'    : 2,
    'properties'      : 0,
    'quotes'          : [2, 'single'],
    'semi'            : 2,
    'sort-keys'       : [2, 'asc', { 'caseSensitive': false }],
    'space-in-parens' : 2,
    'spaced-comment'  : 2,
  },
};
