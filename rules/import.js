'use strict';

module.exports = {
  plugins : ['import'],
  rules   : {
    'import/default'                     : 2,
    'import/first'                       : 2,
    'import/named'                       : 2,
    'import/newline-after-import'        : 2,
    'import/no-anonymous-default-export' : 1,
    'import/no-duplicates'               : 2,
    'import/no-named-default'            : 2,
    'import/no-namespace'                : 2,
    'import/no-webpack-loader-syntax'    : 2,
    'import/prefer-default-export'       : 2,
  },
};
