'use strict';

module.exports = {
  // Leave as map for now in case we ever decide to require more rules on index
  extends : ['./dev'].map(require.resolve),
  rules   : {},
};
