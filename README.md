# esp-presets

Espressive frontend team presets. Currently this project only has eslint presets in it. This may change in the future.

### `eslint`

To include the presets from this repository in an Espressive project, add the repository to your `package.json`:

###### package.json
```
{
	"devDependencies": {
		"@espressive/eslint-config": "https://bitbucket.org/itautosys/esp-presets.git",
	}
}
```

Then add the following to your `.eslintrc` file:

###### .eslintrc
```
{
	"extends": [
		"@espressive/eslint-config"
	],
}
```

You can add these presets to combine with other presets. By default, the above preset exports the Espressive dev presets. If you would like to include different presets in your eslint config, you can reference them as follows:

#### dev (default)
```
{
	"extends": [
		"@espressive/eslint-config/dev"
	],
}
```

#### prod
```
{
	"extends": [
		"@espressive/eslint-config/prod"
	],
}
```

------

#### base only
```
{
	"extends": [
		"@espressive/eslint-config/rules/base"
	],
}
```

#### react plugin rules only
```
{
	"extends": [
		"@espressive/eslint-config/rules/react"
	],
}
```

#### import plugin rules only
```
{
	"extends": [
		"@espressive/eslint-config/rules/import"
	],
}
```