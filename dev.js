'use strict';

module.exports = {
  extends: [
    './rules/base',
    './rules/react',
    './rules/import',
  ].map(require.resolve),
  rules: {},
};
