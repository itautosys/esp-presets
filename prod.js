'use strict';

module.exports = {
  extends: [
    './rules/base',
    './rules/react',
    './rules/import',
  ].map(require.resolve),
  rules: {
    'no-console'  : [2, { allow: ['error', 'warn'] }],
    'no-debugger' : 2,
  },
};
